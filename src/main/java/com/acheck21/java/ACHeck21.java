/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.simpleframework.xml.transform.Transform;
import org.simpleframework.xml.transform.Transformer;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.MultipartBody;

/**
* This class wraps the ACHeck21 REST API to provide an easy to use Java interface. All 
* functions are static and stateless. See the REST service documentation at 
* http://help.acheck21.com/hc/en-us/categories/200315315-Development-Center
*/
public class ACHeck21 
{
    private static String baseUrl = "https://gateway.acheck21.com/GlobalGateway/rest";
    
    /**
     * Payment entry classes. Different processing requirements and rules apply to different entry classes.
     */
    public enum EntryClass
    {
        TEL, PPD, ARC, RCK, CCD, WEB, BOC, POP, C21
    }
    
    /**
     * Indicates a Checking or Savings account
     */
    public enum AccountType
    {
        Checking, Savings
    }
    
    private static String getStringForDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }
    
    /**
     * Creates a check in the ACHeck21 Global Gateway. Wraps a REST call to the
     * CreateCheck REST service. For more details, see the documentation at 
     * http://help.acheck21.com/hc/en-us/articles/203384609-Web-Service-CreateCheck
     * 
     * @param username The global gateway username
     * @param password The global gateway password
     * @param clientID The global gateway client ID
     * @param clientTag Optional string to associate this check with in a client's system.
     * @param individualName The individual making the payment. 
     * @param checkNumber The check number. 
     * @param transitNumber The routing number of the check
     * @param ddaNumber The account number the check is from.
     * @param accountType 
     * @param checkAmount The amount of the check
     * @param entryClass
     * @param frontImage An input stream containing a tiff image of the front of the check
     * @param backImage An input stream containing a tiff image of the back of the check
     * @param micr optional micr line string. Note that if you pass this in, you should set checkNumber, transitNumber, and ddaNumber to null.
     * @param postingDate optional date in the future to post the check. 
     * @param addendum Optional addendum to PPD transactions. Limited to 80 characters.
     * @return The checkID of the newly created check. 
     * @throws UnirestException
     * @throws IOException
     * @throws XPathExpressionException
     */
    public static String createCheck(String username, 
                                   String password, 
                                   String clientID, 
                                   String clientTag, 
                                   String individualName, 
                                   String checkNumber,
                                   String transitNumber,
                                   String ddaNumber,
                                   AccountType accountType,
                                   String checkAmount,
                                   EntryClass entryClass,
                                   InputStream frontImage,
                                   InputStream backImage,
                                   String micr,
                                   Date postingDate,
                                   String addendum) throws UnirestException, IOException, XPathExpressionException
    {
        
        // Build the initial request with the fields that are required to be there. 
        MultipartBody reqBody = Unirest.post(baseUrl + "/check")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .field("ClientID", clientID)
                .field("CheckNumber", checkNumber)
                .field("TransitNumber", transitNumber)
                .field("DDANumber", ddaNumber)
                .field("AccountType", accountType.toString())
                .field("CheckAmount", checkAmount)
                .field("EntryClass", entryClass.toString())
                .basicAuth(username, password);
        
        
        // Check for the presence of optional arguments and add them to the request if they are present.
        if (clientTag != null)
        {
            reqBody.field("ClientTag", clientTag);
        }
        if (individualName != null)
        {
            reqBody.field("IndividualName", individualName);
        }
        if (micr != null)
        {
            reqBody.field("MICR", micr);
        }
        if (postingDate != null)
        {
            // Convert the Java date object into a string that the REST service will understand
            String dateStr = getStringForDate(postingDate);
            reqBody.field("PostingDate", dateStr);
        }
        if (addendum != null)
        {
            reqBody.field("Addendum", addendum);
        }
        
        
        // The check images are optional. If present, we need to convert these into a 
        // base64 encoded string for the HTTP request to the REST service
        if (frontImage != null)
        {
            String frontImage64 = toBase64(frontImage);
            reqBody.field("FrontImage", frontImage64);
        }
        if (backImage != null)
        {
            String backImage64 = toBase64(backImage);
            reqBody.field("RearImage", backImage64);
        }
         
        HttpResponse<String> resp = reqBody.asString();
        
        // The rest endpoint returns the result as a simple xml document. 
        // This is a url to the GetCheck REST endpoint wrapped in <uri> tags.
        // We extract the content between the tags and then use it to extract the CheckID.
        String rawXml = resp.getBody();
        return extractCheckIdFromCreateCheckUrl(extractUrl(rawXml));
    }
    
    /**
     * Finds and returns a list of Checks in the global gateway based on some search criteria. 
     * If the checks haven't been processed, you will need to use findPendingChecksDetails to find them.
     * 
     * This function wraps a call to the FindChecksDetails REST service. Documented at 
     * http://help.acheck21.com/hc/en-us/articles/203285679-Web-Service-FindChecksDetails
     * 
     * @param username global gateway username
     * @param password global gateway passowrd
     * @param clientID global gateway clientID
     * @param query A query containing search criteria to look for checks. The specific format of the query
     *        language can be found at http://help.acheck21.com/hc/en-us/articles/204143405-Concept-ACHeck21-XML-Query-Language
     * @return A List of CheckResults
     * @throws Exception
     */
    public static List<CheckResult> findChecksDetails(String username, String password, String clientID, String query) throws Exception
    {
        return checksQuery(baseUrl + "/checks/" + clientID + "/details?" + query, username, password, clientID, query);
    }

    /**
     * Finds and returns a list of pending checks in the global gateway based on some search criteria. 
     * If the checks have been processed, you will need to use findChecksDetails to find them.
     * 
     * This function wraps a call to the FindPendingChecksDetails REST service. Documented at 
     * http://help.acheck21.com/hc/en-us/articles/203568379-Web-Service-FindPendingChecksDetails
     * 
     * @param username global gateway username
     * @param password global gateway passowrd
     * @param clientID global gateway clientID
     * @param query A query containing search criteria to look for checks. The specific format of the query
     *        language can be found at http://help.acheck21.com/hc/en-us/articles/204143405-Concept-ACHeck21-XML-Query-Language
     * @return A List of CheckResults
     * @throws Exception
     */
    public static List<CheckResult> findPendingChecksDetails(String username, String password, String clientID, String query) throws Exception
    {
       return checksQuery(baseUrl + "/checks/" + clientID + "/pending/details?" + query, username, password, clientID, query);
    }
    
    /**
     * Sends a batch to the global gateway. Uses a data object to represent the data contained in a batch file
     * and serializes that into the proper format required by the global gateway. 
     * 
     * This function wraps a call to the SendBatch REST service. Documented at
     * http://help.acheck21.com/hc/en-us/articles/204252565-Web-Service-SendBatch
     * 
     * @param username global gateway username
     * @param password global gateway passowrd
     * @param clientID global gateway clientID
     * @param batch Object that represents the information in a batch file. 
     * @return true if the batch was accepted, false otherwise.
     * @throws Exception
     */
    public static boolean sendBatch(String username, String password, String clientID, Batch batch) throws Exception
    {
        // Set up the xml serializer. 
        RegistryMatcher m = new RegistryMatcher();
        m.bind(EntryClass.class, new EntryClassTransform());
        m.bind(Date.class, new BatchDateTransform());
        Serializer serializer = new Persister(m);
        
        // Do the serialization of the Batch object to xml
        StringWriter writer = new StringWriter();
        serializer.write(batch, writer);
        
        // Convert the xml to base64
        byte[] bytes = StringUtils.getBytesUtf8(writer.toString());
        String batch64 = Base64.getEncoder().encodeToString(bytes);
        
        // Make the REST call using our base64 encoded xml
        HttpResponse<String> resp = Unirest.post(baseUrl + "/batch/" + clientID)
               .field("Base64EncodedBatch", batch64)
               .field("Filename", "batch.xml")
               .basicAuth(username, password)
               .asString();
        
        return resp.getStatus() == 204;
    }
    
    /**
     * Deletes a check from the global gateway.
     * 
     * This function wraps a call to the DeleteCheck REST service. Documented at
     * http://help.acheck21.com/hc/en-us/articles/204501455-Web-Service-DeleteCheck
     * 
     * @param username global gateway username
     * @param password global gateway passowrd
     * @param checkID The id of the check to delete
     * @return true if the delete was successful.
     * @throws UnirestException
     */
    public static boolean deleteCheck(String username, String password, String checkID) throws UnirestException
    {
        HttpResponse<String> resp = Unirest.delete(baseUrl + "/check/" + checkID).basicAuth(username, password).asString();
        return resp.getStatus() == 204;
    }
    
    /**
     * Convenience method to convert an input stream to a base64 string
     * 
     * @param stream
     * @return
     * @throws IOException
     */
    static String toBase64(InputStream stream) throws IOException
    {
        byte[] byteArray = IOUtils.toByteArray(stream);
        String stream64 = Base64.getEncoder().encodeToString(byteArray);
        return stream64;
    }
    
    /**
     * Used by {@link ACHeck21#findChecksDetails(String, String, String, String)} and 
     * {@link ACHeck21#findPendingChecksDetails(String, String, String, String)} to make 
     * the REST call to the appropriate REST service
     * 
     * @param url the url to make the REST call to.
     * @param username global gateway username
     * @param password global gateway password
     * @param clientID global gateway clientID
     * @param query The query to pass to the REST service
     * @return list of CheckResults.
     * @throws Exception
     */
    private static List<CheckResult> checksQuery(String url, String username,
            String password, String clientID, String query)
            throws Exception
    {
        HttpResponse<String> resp = Unirest.get(url)
                                           .basicAuth(username, password)
                                           .asString();
        String rawXml = resp.getBody();
        if (resp.getStatus() == 204 || rawXml == null)
        {
            return Collections.emptyList();
        }
        
        rawXml = rawXml.startsWith("\ufeff") ? rawXml.substring(1) : rawXml;
        return extractCheckResults(rawXml);
    }
    
    /**
     * Convenience method to deserialize the xml returned from the global gateway from 
     * a Find*ChecksDetails REST call.
     * 
     * @param rawXml
     * @return
     * @throws Exception
     */
    private static List<CheckResult> extractCheckResults(String rawXml) throws Exception
    {
        RegistryMatcher m = new RegistryMatcher();
        m.bind(EntryClass.class, new EntryClassTransform());
        m.bind(Date.class, new DateTransform());
        Serializer serializer = new Persister(m);
        CheckResultList resultList = serializer.read(CheckResultList.class, rawXml);
        return resultList.getChecks();
    }
    
    /**
     * Used by {@link ACHeck21#createCheck} to extract the checkID from the url returned
     * by the CreateCheck REST service.
     * 
     * @param url
     * @return
     * @throws MalformedURLException
     */
    private static String extractCheckIdFromCreateCheckUrl(String url) throws MalformedURLException
    {
        URL urlObj = new URL(url);
        String[] split = urlObj.getPath().split("/");
        return split[split.length - 1];
    }
    
    /**
     * Used by {@link ACHeck21#createCheck} to extract the url from the xml returned
     * by the CreateCheck REST service. The url is then passed to {@link ACHeck21#extractCheckIdFromCreateCheckUrl(String)}
     * 
     * @param rawXml
     * @return
     * @throws XPathExpressionException
     */
    private static String extractUrl(String rawXml) throws XPathExpressionException
    {
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        
        InputSource xmlSource = new InputSource(new StringReader(rawXml));
        String url = xpath.evaluate("/uri/text()", xmlSource);
        return url;
    }
    
    
    @Root(name="Checks")
    private static class CheckResultList
    {
        @ElementList(inline=true)
        List<CheckResult> checks;
        
        public void setChecks(List<CheckResult> checks)
        {
            this.checks = checks;
        }
        
        public List<CheckResult> getChecks()
        {
            return this.checks;
        }
    }
    private static class BatchDateTransform implements Transform<Date>
    {
        private static SimpleDateFormat batchFormat = new SimpleDateFormat("yyyy/MM/dd");
        public Date read(String value) throws Exception
        {
            return batchFormat.parse(value);
        }

        public String write(Date value) throws Exception
        {
            return batchFormat.format(value);
        }
        
    }
    private static class DateTransform implements Transform<Date>
    {
        private static SimpleDateFormat uploadDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        private static SimpleDateFormat otherDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        public Date read(String value) throws Exception 
        {
            try
            {
                return uploadDateFormat.parse(value);
            }
            catch (ParseException e)
            {
                return otherDateFormat.parse(value);
            }
        }

        public String write(Date value) throws Exception
        {
            return getStringForDate(value);
        }
        
    }
    
    private static class EntryClassTransform implements Transform<EntryClass>
    {

        public EntryClass read(String arg) throws Exception
        {
            if (arg.equals("937"))
            {
                return EntryClass.C21;
            }
            return EntryClass.valueOf(arg);
        }

        public String write(EntryClass arg) throws Exception
        {
            return arg.toString();
        }
        
    }
}
