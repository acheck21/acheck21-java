/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


/**
 * This class is used to hold data that is transmitted in an ACHeck21 batch file.
 * Its purpose is to simplify converting the data into xml to
 * make a call to the SendBatch REST service. 
 */
@Root(name="ACHFile")
public class Batch
{
    @Element(name="ClientID")
    private String clientID;
    
    @Element(name="UploadDate")
    private Date uploadDate;
    
    @Element(name="SeqNbr")
    private String seqNbr;
    
    @ElementList(name="Transactions")
    private ArrayList<BatchTransaction> transactions;
    
    @Element(name="EntryCount")
    private int entryCount;
    
    @Element(name="XmitTotal")
    private double xmitTotal;
    
    /**
     * Constructs a batch file. 
     * 
     * @param clientID The id of the client that
     * @param sequenceNumber A unique sequence number for the batch.
     */
    public Batch(String clientID, String sequenceNumber)
    {
        this.clientID = clientID;
        this.seqNbr = sequenceNumber;
        this.transactions = new ArrayList<BatchTransaction>();
        this.uploadDate = new Date();
    }
    
    /**
     * Adds a transaction to the batch. 
     * @param transaction The transaction to add.
     */
    public void addTransaction(BatchTransaction transaction)
    {
        this.transactions.add(transaction);
        this.xmitTotal += transaction.getCheckAmount();
        this.entryCount = this.transactions.size();
    }
    
}
