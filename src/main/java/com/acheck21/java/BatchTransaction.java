/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import com.acheck21.java.ACHeck21.EntryClass;

/**
 * This class represents the data contained in a single transaction to be sent 
 * in a batch to the ACHeck21 global gateway.
 *
 */
@Root(name="Transaction")
public class BatchTransaction
{
    
    @Element(name="ClientTag")
    private String clientTag;
    
    @Element(name="EntryClassCd")
    private EntryClass entryClass;
    
    @Element(name="TransactionCd")
    private int transactionCode;
    
    @Element(name="TransitNbr")
    private String transitNumber;
    
    @Element(name="DDANbr")
    private String ddaNumber;
    
    @Element(name="CheckNbr")
    private String checkNumber;
    
    @Element(name="CheckAmt")
    private double checkAmount;
    
    @Element(name="IndividualName", required=false)
    private String individualName;
    
    @Element(name="CompanyName", required=false)
    private String companyName;
    
    @Element(name="CompanyEntryDescr", required=false)
    private String companyEntryDescription;
    
    @Element(name="PhoneNumber", required=false)
    private String phoneNumber;
    
    @Element(name="DLNumber", required=false)
    private String dlNumber;
    
    @Path("Addenda")
    @Element(name="Addendum", required=false)
    private String addendum;
    
    @Element(name="FrontImage", required=false)
    private String frontImage;
    
    @Element(name="RearImage", required=false)
    private String rearImage;
    
    @Element(name="RCCInfo", required=false)
    private RCCInfo rccInfo;

    /**
     * Creates a new BatchTransaction.
     * 
     * @param clientTag required field. unique transaction identifier supplied by the merchant.
     * @param entryClass The entry class of the payment
     * @param transactionCode Type of transaction
     * @param transitNumber The routing number of the check.
     * @param ddaNumber The account number of the check
     * @param checkNumber The check number
     * @param checkAmount The check amount
     * @param individualName The person writing the check.
     * @param companyName The name of the company 
     * @param companyEntryDescription short description of the transaction
     * @param phoneNumber The phone number of the person writing the check
     * @param dlNumber Driver's license number of the person writing the check.
     * @param addendum Formatted PPD addendum. Only used for PPD entry class transactions.
     * @param frontImage InputStream containing a binary tiff image of the front of the check.
     * @param rearImage InputStream containing a binary tiff image of the back of the check.
     * @param rccInfo Info for RCC entry class transactions.
     * @throws IOException
     */
    public BatchTransaction(String clientTag, EntryClass entryClass,
            TransactionCode transactionCode, String transitNumber, String ddaNumber,
            String checkNumber, double checkAmount, String individualName,
            String companyName, String companyEntryDescription,
            String phoneNumber, String dlNumber, String addendum,
            InputStream frontImage, InputStream rearImage, RCCInfo rccInfo) throws IOException
    {
        super();
        this.clientTag = clientTag;
        this.entryClass = entryClass;
        this.transactionCode = transactionCode.getNum();
        this.transitNumber = transitNumber;
        this.ddaNumber = ddaNumber;
        this.checkNumber = checkNumber;
        this.checkAmount = checkAmount;
        this.individualName = individualName;
        this.companyName = companyName;
        this.companyEntryDescription = companyEntryDescription;
        this.phoneNumber = phoneNumber;
        this.dlNumber = dlNumber;
        this.addendum = addendum;
        this.frontImage = frontImage != null ? ACHeck21.toBase64(frontImage) : null;
        this.rearImage = rearImage != null ? ACHeck21.toBase64(rearImage) : null;
        this.rccInfo = rccInfo;
    }
    
    
    public String getClientTag()
    {
        return clientTag;
    }

    public EntryClass getEntryClass()
    {
        return entryClass;
    }

    public int getTransactionCode()
    {
        return transactionCode;
    }

    public String getTransitNumber()
    {
        return transitNumber;
    }

    public String getDdaNumber()
    {
        return ddaNumber;
    }

    public String getCheckNumber()
    {
        return checkNumber;
    }

    public double getCheckAmount()
    {
        return checkAmount;
    }

    public String getIndividualName()
    {
        return individualName;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public String getCompanyEntryDescription()
    {
        return companyEntryDescription;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public String getDlNumber()
    {
        return dlNumber;
    }

    public String getAddendum()
    {
        return addendum;
    }

    public String getFrontImage()
    {
        return frontImage;
    }

    public String getRearImage()
    {
        return rearImage;
    }

    public RCCInfo getRccInfo()
    {
        return rccInfo;
    }

    public enum TransactionCode
    {
        
        CreditToChecking(22),
        DebitFromChecking(27),
        CreditToSavings(32),
        DebitFromSavings(37);
        
        private int num;
        
        TransactionCode(int num)
        {
            this.num = num;
        }
        public int getNum()
        {
            return num;
        }
    }
    
    
}
