/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * The class represents the data contained in a single check result returned from
 * the ACHeck21 FindCheckDetails and FindPendingChecksDetails REST services.
 *
 */
@Root(name="CheckInfo", strict=false)
public class CheckResult
{
    @Element(name="CheckID")
    private String checkID;
    
    @Element(name="CheckNumber")
    private String checkNumber;
    
    @Element(name="ClientID")
    private String clientID;
    
    @Element(name="UploadDate")
    private Date uploadDate;
    
    @Element(name="IndividualName", required=false)
    private String individualName;
    
    @Element(name="TransitNumber")
    private String transitNumber;
    
    @Element(name="DDANumber")
    private String ddaNumber;
    
    @Element(name="AccountType")
    private ACHeck21.AccountType accountType;
    
    @Element(name="CheckAmount")
    private String checkAmount;
    
    @Element(name="ClientTag", required=false)
    private String clientTag;
    
    @Element(name="EntryClass")
    private ACHeck21.EntryClass entryClass;
    
    @Element(name="PostingDate")
    private Date postingDate;
    
    @Path("Presentments/Presentment")
    @Element(name="ToFedDate")
    private Date toFedDate;
    
    @Element(name="Addenda", required=false)
    private String addenda;
    
    @Element(name="ReturnStatus", required=false)
    private String returnStatus;
    
    public String getCheckID()
    {
        return checkID;
    }
    public void setCheckID(String checkID)
    {
        this.checkID = checkID;
    }
    public String getClientID()
    {
        return clientID;
    }
    public void setClientID(String clientID)
    {
        this.clientID = clientID;
    }
    public Date getUploadDate()
    {
        return uploadDate;
    }
    public void setUploadDate(Date uploadDate) throws ParseException
    {
        this.uploadDate = uploadDate;
    }
    public String getIndividualName()
    {
        return individualName;
    }
    public void setIndividualName(String individualName)
    {
        this.individualName = individualName;
    }
    public String getTransitNumber()
    {
        return transitNumber;
    }
    public void setTransitNumber(String transitNumber)
    {
        this.transitNumber = transitNumber;
    }
    public String getDdaNumber()
    {
        return ddaNumber;
    }
    public void setDdaNumber(String ddaNumber)
    {
        this.ddaNumber = ddaNumber;
    }
    public ACHeck21.AccountType getAccountType()
    {
        return accountType;
    }
    public void setAccountType(String accountType)
    {
        this.accountType = ACHeck21.AccountType.valueOf(accountType);
    }
    public String getCheckAmount()
    {
        return checkAmount;
    }
    public void setCheckAmount(String checkAmount)
    {
        this.checkAmount = checkAmount;
    }
    public String getClientTag()
    {
        return clientTag;
    }
    public void setClientTag(String clientTag)
    {
        this.clientTag = clientTag;
    }
    public ACHeck21.EntryClass getEntryClass()
    {
        return entryClass;
    }
    public void setEntryClass(String entryClass)
    {
        this.entryClass = ACHeck21.EntryClass.valueOf(entryClass);
    }
    public Date getPostingDate()
    {
        return postingDate;
    }
    public void setPostingDate(Date postingDate) throws ParseException
    {
        this.postingDate = postingDate;//otherDateFormat.parse(postingDate);
    }
    public Date getToFedDate()
    {
        return toFedDate;
    }
    public void setToFedDate(Date toFedDate) throws ParseException
    {
        this.toFedDate = toFedDate;//otherDateFormat.parse(toFedDate);
    }
    public String getAddenda()
    {
        return addenda;
    }
    public void setAddenda(String addenda)
    {
        this.addenda = addenda;
    }
    public String getReturnStatus()
    {
        return returnStatus;
    }
    public void setReturnStatus(String returnStatus)
    {
        this.returnStatus = returnStatus;
    }
    public String getCheckNumber()
    {
        return checkNumber;
    }
    public void setCheckNumber(String checkNumber)
    {
        this.checkNumber = checkNumber;
    }
    
    
}
