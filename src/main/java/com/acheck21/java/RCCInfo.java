/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
/**
 * Represents additional data used for RCC entry class transactions
 */
@Root
public class RCCInfo
{
    @Element(name="Line1", required=false)
    private String rccLine1;
    
    @Element(name="Line2", required=false)
    private String rccLine2;
    
    @Element(name="Line3", required=false)
    private String rccLine3;
    
    @Element(name="Line4", required=false)
    private String rccLine4;
    
    @Element(name="Line5", required=false)
    private String rccLine5;
    
    @Element(name="Signature", required=false)
    private String rccSignature;
    
    @Element(name="Endorse1", required=false)
    private String rccEndorse1;
    
    @Element(name="Endorse2", required=false)
    private String rccEndorse2;
    
    @Element(name="Endorse3", required=false)
    private String rccEndorse3;
    
    @Element(name="Endorse4", required=false)
    private String rccEndorse4;

    /**
     * Constructs an RCCInfo object
     * 
     * @param rccLine1 text to print on the top line of the address block in RCC transactions
     * @param rccLine2 text to print on the second line of the address block in RCC transactions
     * @param rccLine3 text to print on the third line of the address block in RCC transactions
     * @param rccLine4 text to print on the fourth line of the address block in RCC transactions
     * @param rccLine5 text to print on the fifth line of the address block in RCC transactions
     * @param rccSignature text to print on the signature line for RCC transactions.
     * @param rccEndorse1 text to print on the first line of the endorsement on the back of the RCC check
     * @param rccEndorse2 text to print on the second line of the endorsement on the back of the RCC check
     * @param rccEndorse3 text to print on the third line of the endorsement on the back of the RCC check
     * @param rccEndorse4 text to print on the fourth line of the endorsement on the back of the RCC check
     */
    public RCCInfo(String rccLine1, String rccLine2, String rccLine3,
            String rccLine4, String rccLine5, String rccSignature,
            String rccEndorse1, String rccEndorse2, String rccEndorse3,
            String rccEndorse4)
    {
        this.rccLine1 = rccLine1;
        this.rccLine2 = rccLine2;
        this.rccLine3 = rccLine3;
        this.rccLine4 = rccLine4;
        this.rccLine5 = rccLine5;
        this.rccSignature = rccSignature;
        this.rccEndorse1 = rccEndorse1;
        this.rccEndorse2 = rccEndorse2;
        this.rccEndorse3 = rccEndorse3;
        this.rccEndorse4 = rccEndorse4;
    }

    public String getRccLine1()
    {
        return rccLine1;
    }

    public String getRccLine2()
    {
        return rccLine2;
    }

    public String getRccLine3()
    {
        return rccLine3;
    }

    public String getRccLine4()
    {
        return rccLine4;
    }

    public String getRccLine5()
    {
        return rccLine5;
    }

    public String getRccSignature()
    {
        return rccSignature;
    }

    public String getRccEndorse1()
    {
        return rccEndorse1;
    }

    public String getRccEndorse2()
    {
        return rccEndorse2;
    }

    public String getRccEndorse3()
    {
        return rccEndorse3;
    }

    public String getRccEndorse4()
    {
        return rccEndorse4;
    }
    
    
}
