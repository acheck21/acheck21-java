/**
 * Copyright (c) 2015 Diversified Check Solutions, LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.acheck21.java;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.acheck21.java.ACHeck21.AccountType;
import com.acheck21.java.ACHeck21.EntryClass;
import com.acheck21.java.BatchTransaction.TransactionCode;
import com.mashape.unirest.http.exceptions.UnirestException;

public class TestACHeck21
{
    private static String username = System.getProperty("test.username");
    private static String password = System.getProperty("test.password");
    private static String clientID = System.getProperty("test.clientId");
    
            
    @Test
    public void testSimpleCheck() throws UnirestException, IOException, XPathExpressionException
    {
        String checkID = createSimpleCheck();
        
        assertTrue(ACHeck21.deleteCheck(username, password, checkID));
        
    }
    
    @Test
    public void testCheckWithPostingDate() throws XPathExpressionException, MalformedURLException, UnirestException, IOException 
    {
        String transitNumber = "063100277";
        String ddaNumber = "12345678912345";
        ACHeck21.AccountType accountType = AccountType.Checking;
        String checkAmount = "132.10";
        ACHeck21.EntryClass entryClass = EntryClass.WEB;
        InputStream frontImage = null;
        InputStream backImage = null;
        String micr = null;
        Date postingDate = new Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
        String addendum = null;
        
        String checkID = doCreateCheck(null, null, transitNumber, ddaNumber, accountType, checkAmount, entryClass, frontImage, backImage, micr, postingDate, addendum);
        
        assertTrue(ACHeck21.deleteCheck(username, password, checkID));
    }
    
    @Test
    public void testCheckWithImages() throws XPathExpressionException, MalformedURLException, UnirestException, IOException 
    {
        String transitNumber = "063100277";
        String ddaNumber = "12345678912345";
        ACHeck21.AccountType accountType = AccountType.Checking;
        String checkAmount = "132.10";
        ACHeck21.EntryClass entryClass = EntryClass.C21;
        InputStream frontImage = getClass().getResourceAsStream("/test-images/front.tiff");
        InputStream backImage = getClass().getResourceAsStream("/test-images/back.tiff");
        String micr = null;
        Date postingDate = new Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
        String addendum = null;
        
        String checkID = doCreateCheck(null, null, transitNumber, ddaNumber, accountType, checkAmount, entryClass, frontImage, backImage, micr, postingDate, addendum);
        
        assertTrue(ACHeck21.deleteCheck(username, password, checkID));
    }
    
    @Test
    public void testSimpleFindPendingDetails() throws Exception
    {
        String checkID = createSimpleCheck();
        
        List<CheckResult> checks = ACHeck21.findPendingChecksDetails(username, password, clientID, "CheckID=equal," + checkID);
        
        assertEquals(1, checks.size());
    }
    
    @Test
    public void testFindChecksDetails() throws Exception
    {        
        List<CheckResult> checks = ACHeck21.findChecksDetails(username, password, clientID, "CheckID=equal,80256940");
        
        assertNotNull(checks);   
        
        assertEquals(1, checks.size());
        CheckResult check = checks.get(0);
        assertEquals("80256940", check.getCheckID());
        assertEquals(1430280000000L, check.getUploadDate().getTime());
        assertEquals("TestAcct2", check.getIndividualName());
        assertEquals("12358785757", check.getCheckNumber());
        assertEquals("012345672", check.getTransitNumber());
        assertEquals("1239873995", check.getDdaNumber());
        assertEquals(AccountType.Checking, check.getAccountType());
        assertEquals("2.00", check.getCheckAmount());
        assertEquals("255,2,0", check.getClientTag());
        assertEquals(EntryClass.C21, check.getEntryClass());
        assertEquals(1430280000000L,check.getPostingDate().getTime());
        assertEquals(1430280000000L,check.getToFedDate().getTime());
    }
    
    @Test
    public void testSendBatchWithSingleItem() throws Exception
    {
        
        String sequenceNumber = String.valueOf((int)(Math.random() * 10000000));
        String tag = sequenceNumber;
        String transitNumber = "063100277";
        String ddaNumber = "12345678912345";
        String checkNumber = String.valueOf(((int)(Math.random() * 10000000)));
        double checkAmount = 100.12;
        String individualName = "Nobody J. Smith";
        BatchTransaction transaction = new BatchTransaction(tag, 
                                                            EntryClass.WEB, 
                                                            TransactionCode.DebitFromChecking, 
                                                            transitNumber,
                                                            ddaNumber, 
                                                            checkNumber,
                                                            checkAmount, 
                                                            individualName,
                                                            "ACME inc.",
                                                            "Nothing",
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null);
        Batch batch = new Batch(clientID, sequenceNumber);
        batch.addTransaction(transaction);
        
        assertTrue(ACHeck21.sendBatch(username, password, clientID, batch));
    }
    
    private String createSimpleCheck() throws UnirestException, IOException,
            XPathExpressionException, MalformedURLException
    {
        String clientTag = null;
        String individualName = null;
        
        String transitNumber = "063100277";
        String ddaNumber = "12345678912345";
        ACHeck21.AccountType accountType = AccountType.Checking;
        String checkAmount = "132.10";
        ACHeck21.EntryClass entryClass = EntryClass.WEB;
        InputStream frontImage = null;
        InputStream backImage = null;
        String micr = null;
        Date postingDate = null;
        String addendum = null;
        
        String checkID = doCreateCheck(clientTag, individualName, transitNumber,
                ddaNumber, accountType, checkAmount, entryClass, frontImage,
                backImage, micr, postingDate, addendum);
        return checkID;
    }

    private String doCreateCheck(String clientTag, String individualName, String transitNumber, String ddaNumber,
            ACHeck21.AccountType accountType, String checkAmount,
            ACHeck21.EntryClass entryClass, InputStream frontImage,
            InputStream backImage, String micr, Date postingDate,
            String addendum) throws UnirestException, IOException,
            XPathExpressionException, MalformedURLException
    {
        String checkNumber = String.valueOf(((int)(Math.random() * 10000)));
        String checkId = ACHeck21.createCheck(username, 
                                         password, 
                                         clientID, 
                                         clientTag,
                                         individualName, 
                                         checkNumber, 
                                         transitNumber, 
                                         ddaNumber, 
                                         accountType, 
                                         checkAmount, 
                                         entryClass, 
                                         frontImage,
                                         backImage, 
                                         micr, 
                                         postingDate, 
                                         addendum);
        return checkId;
        
    }
}
